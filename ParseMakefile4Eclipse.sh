#!/bin/bash
#
# icechao@gmail.com
#
# version 1.0

INBLOCK=0
MAKEFILE=""
EXPORTFILE="/tmp/nrf.tmp"
TEMPFILE="/tmp/parse.tmp"
IMPORTFILE="/tmp/nrf.tmp"

USAGE(){
  echo
  echo "    nRF52 Example Makefile to Eclipse Parser"
  echo 
  echo "    USAGE:"
  echo "        `basename $0` [path/to/nrf52/example/project/Makefile]"
  echo 
  echo "    Steps:" 
  echo "        1. In Eclipse, create a new C/C++ Makefile porject with existing code"
  echo "        2. In Eclipse project properties,"
  echo "           choose C/C++ Build --> Settings --> Devices"
  echo "        3. run the script"
  echo "        4. In Eclipse project properties,"
  echo "           import the ${IMPORTFILE} to the Settings..." 
  echo "        5. Reflesh the project in Eclipse."
  echo
  exit
}

# make sure input path is given
[ $# -ne 1 ] && USAGE

# make sure the path and Makefile exist
[ -d "${1}" -a -f "${1}/Makefile" ] && MAKEFILE="${1}/Makefile"
[ -f "${1}" ] && MAKEFILE="${1}"
[ -z "${MAKEFILE}" ] && { echo "[EE] Cannot find the Makefile"; USAGE; }
PATHNAME=`dirname "${MAKEFILE}"`

# find the Eclipse .project file
[ -f "${PATHNAME}/.project" ] && { PROJFILE="${PATHNAME}/.project"; } || { echo "[EE] Missing the Eclipse project files."; USAGE; }
[ -f "${PATHNAME}/.cproject" ] && { CPROJFILE="${PATHNAME}/.cproject"; } || { echo "[EE] Missing the Eclipse cproject files."; USAGE; }


echo "Parsing: ${MAKEFILE}"
PROJ_NAME=`grep "PROJECT_NAME.*:=" ${MAKEFILE} | cut -d= -f2 | tr -d '[:space:]'`
SDK_ROOT=`grep "SDK_ROOT.*:=" ${MAKEFILE} | cut -d= -f2 | tr -d '[:space:]'`
PROJ_DIR=`grep "PROJ_DIR.*:=" ${MAKEFILE} | cut -d= -f2 | tr -d '[:space:]'`
echo "Found ${PROJ_NAME}"
echo "SDK_ROOT = ${SDK_ROOT}"
echo "PROJ_DIR = ${PROJ_DIR}"
PROJ_DIR_NUM=`echo ${PROJ_DIR} | awk -F'/' '{print NF}'`
SDK_ROOT_NUM=`echo ${SDK_ROOT} | awk -F'/' '{print NF}'`


# parse C include path
echo
echo '<?xml version="1.0" encoding="UTF-8"?>' > ${IMPORTFILE}
echo '<cdtprojectproperties>' >> ${IMPORTFILE}
echo '<section name="org.eclipse.cdt.internal.ui.wizards.settingswizards.IncludePaths">' >> ${IMPORTFILE}
echo '<language name="C Source File">' >> ${IMPORTFILE}

HEADER="INC_FOLDERS"
while IFS=$'\n\r' read -r line; do
  if [[ $INBLOCK -ne 0 ]]; then 
    if [[ "$line" != *"\\" ]]; then
      INBLOCK=0
    else
      line=$(echo -e "${line}" | sed -e 's/[[:space:]]*\\//' -e 's/^[[:space:]]*//')
      line=$(echo -e "${line}" | sed -e 's/(/{/' -e 's/)/}/')
      line=$(echo -e "${line}" | sed -e 's/^\.\./${CWD}\/../')
      line="<includepath>${line}</includepath>"
      echo $line >> ${IMPORTFILE}
      #echo $line | awk '{split($line, t, "/"); print t[1] }'
    fi
  elif [[ "$line" == "${HEADER}"* ]]; then
    INBLOCK=1
  fi
done < ${MAKEFILE}
echo '</language>' >> ${IMPORTFILE}
echo '</section>' >> ${IMPORTFILE}

# parse ASM symbols
echo '<section name="org.eclipse.cdt.internal.ui.wizards.settingswizards.Macros">' >> ${IMPORTFILE}

echo '<language name="Assembly Source File">' >> ${IMPORTFILE}

awk '/ASMFLAGS.*-D/{
  gsub(/ASMFLAGS.*-D/,"");  
  gsub(/\r/,"");  
  gsub(/\n/,"");  
  split($0, t, "=");
  print "<macro>"
  if (t[2] != "")
    printf "<name>%s</name><value>%s</value>\n", t[1], t[2];
  else
    printf "<name>%s</name><value/>\n", t[1];
  print "</macro>"
}' ${MAKEFILE} >> ${IMPORTFILE}

echo '</language>' >> ${IMPORTFILE}

# parse C symbols
echo '<language name="C Source File">' >> ${IMPORTFILE}

awk '/CFLAGS.*-D/{
  gsub(/CFLAGS.*-D/,"");  
  gsub(/\r/,"");  
  gsub(/\n/,"");  
  split($0, t, "=");
  print "<macro>"
  if (t[2] != "")
    printf "<name>%s</name><value>%s</value>\n", t[1], t[2];
  else
    printf "<name>%s</name><value/>\n", t[1];
  print "</macro>"
}' ${MAKEFILE} >> ${IMPORTFILE}

echo '</language>' >> ${IMPORTFILE}
echo '</section>' >> ${IMPORTFILE}
echo '</cdtprojectproperties>' >> ${IMPORTFILE}


####################################################
# parse C files
####################################################

CPROJ_ID=`grep "project id=" ${CPROJFILE} | tr -d '[:space:]' | cut -d\" -f2 | cut -d. -f1`


##########################
# Work on .cproject file #
##########################
MATCH="<externalSettings\/>"
awk "/${MATCH}|<macros>/ {exit} {print}" ${CPROJFILE} > ${TEMPFILE}
echo "<macros>" >> ${TEMPFILE}
echo "<stringMacro name=\"SDK_ROOT\" type=\"VALUE_PATH_ANY\" value=\"\${PWD}/${SDK_ROOT}\"/>" >> ${TEMPFILE}
echo "<stringMacro name=\"PROJ_DIR\" type=\"VALUE_PATH_ANY\" value=\"\${PWD}/${PROJ_DIR}\"/>" >> ${TEMPFILE}
echo "</macros>" >> ${TEMPFILE}
awk "/${MATCH}/,0" ${CPROJFILE} >> ${TEMPFILE}
[ "${PROJ_NAME}" == "${CPROJ_ID}" ] || sed -i "s/${CPROJ_ID}/${PROJ_NAME}/g" ${TEMPFILE}
mv ${CPROJFILE} "${CPROJFILE}.bak"
cp ${TEMPFILE} ${CPROJFILE}

#########################
# Work on .project file #
#########################
awk "{print} /<\/natures>/ {exit}" ${PROJFILE} > ${TEMPFILE}

echo "<linkedResources>" >> ${TEMPFILE}
HEADER="SRC_FILES"
while IFS=$'\n\r' read -r line; do
  if [[ $INBLOCK -ne 0 ]]; then 
    if [[ "$line" != *"\\" ]]; then
      INBLOCK=0
    else
      line=$(echo -e "${line}" | sed -e 's/[[:space:]]*\\//' -e 's/^[[:space:]]*//')
      line=$(echo -e "${line}" | sed -e 's/(//' -e 's/)//' -e 's/\$//')
      name=${line##*/}
      libs=${line%${name}}
      IFS="/" read -r -a array <<< "$libs"
      if (( ${#array[@]} > 1 ));then
        path=${array[1]}
      else
        path="Application"
      fi

	echo "    <link>
        <name>${path}</name>
        <type>2</type>
        <locationURI>virtual:/virtual</locationURI>
    </link>" >> ${TEMPFILE}

        echo "    <link>
        <name>config</name>
        <type>2</type>
        <locationURI>PARENT-1-PROJECT_LOC/config</locationURI>
    </link>" >> ${TEMPFILE}

      for element in "${array[@]:2}";do
        path="$path/$element"
	echo "    <link>
        <name>${path}</name>
        <type>2</type>
        <locationURI>virtual:/virtual</locationURI>
    </link>" >> ${TEMPFILE}
      done

      echo "    <link>
        <name>${path}/${name}</name>
        <type>1</type>
        <locationURI>${line}</locationURI>
    </link>" >> ${TEMPFILE}
    fi
  elif [[ "$line" == "${HEADER}"* ]]; then
    INBLOCK=1
  fi
done < ${MAKEFILE}
echo "</linkedResources>" >> ${TEMPFILE}

echo "<variableList>" >> ${TEMPFILE}
echo "<variable>" >> ${TEMPFILE}
echo "<name>SDK_ROOT</name>" >> ${TEMPFILE}
echo "<value>$%7BPARENT-${SDK_ROOT_NUM}-PROJECT_LOC%7D</value>" >> ${TEMPFILE}
echo "</variable>" >> ${TEMPFILE}
echo "<variable>" >> ${TEMPFILE}
echo "<name>PROJ_DIR</name>" >> ${TEMPFILE}
echo "<value>$%7BPARENT-${PROJ_DIR_NUM}-PROJECT_LOC%7D</value>" >> ${TEMPFILE}
echo "</variable>" >> ${TEMPFILE}
echo "</variableList>" >> ${TEMPFILE}
echo "</projectDescription>" >> ${TEMPFILE}

[ "${PROJ_NAME}" == "${CPROJ_ID}" ] || sed -i.bak "s/${CPROJ_ID}/${PROJ_NAME}/g" ${TEMPFILE}
mv ${PROJFILE} "${PROJFILE}.bak"
cp ${TEMPFILE} ${PROJFILE}

echo "Done"

# call the gedit:
#gedit --new-window ${TEMPFILE} ${PROJFILE} ${EXPORTFILE} &
