## Reference
* [Nordic Developer Zone](https://devzone.nordicsemi.com/tutorials/)
* [Development with GCC and Eclipse](https://devzone.nordicsemi.com/tutorials/7/) 

## Toolchain Setup
1. Create the environment file:

    ```shell
    # go to your nRF52 SDK INSTALL directory.
    mkdir -p ~/git/nrf52/sdk && cd ~/git/nrf52/sdk/
    echo '#!/bin/bash
	
    SDK_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	
    echo $PATH | grep -q "/opt/gcc-arm-none-eabi-6_2-2016q4/bin" || export PATH="/opt/gcc-arm-none-eabi-6_2-2016q4/bin:${PATH}"
    echo $PATH | grep -q "${SDK_ROOT}/mergehex" || export PATH="${SDK_ROOT}/mergehex:${PATH}"
    echo $PATH | grep -q "${SDK_ROOT}/nrfjprog" || export PATH="${SDK_ROOT}/nrfjprog:${PATH}"
    
    PS1="\[\033[01;31m\]\u@nrf5\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ "
    ' > setenv.sh
    source setenv.sh
    ```
2. Install some essential files
    ```shell
	sudo apt-get install build-essential checkinstall
    ```
3. Go to the folder containing the installation files
    ```shell
	cd /path/to/INIT_nrf5_sdk/
    ```
4. [gcc-arm-none-eabi-6_2-2016q4-20161216-linux.tar.bz2](https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/6-2016q4/gcc-arm-none-eabi-6_2-2016q4-20161216-linux.tar.bz2) 
    ```shell
	sudo tar xjvf gcc-arm-none-eabi-*-20161216-linux.tar.bz2 -C /opt/
    ```
5. [JLink_Linux_V612j_x86_64.tgz](https://www.segger.com/downloads/jlink/JLink_Linux_V612j_x86_64.tgz) 
    ```shell
	sudo sh -c "mkdir -p /opt/SEGGER && \
	tar xzvf JLink_Linux_V612j_x86_64.tgz -C /opt/SEGGER && \
	ln -fsrn /opt/SEGGER/JLink_Linux_V612j_x86_64 /opt/SEGGER/JLink && \
	cp /opt/SEGGER/JLink/99-jlink.rules /etc/udev/rules.d/"
    ```
6. [nrfutil](https://codeload.github.com/NordicSemiconductor/pc-nrfutil/zip/master)
    ```shell
	sudo pip install nrfutil
    ```
7. [nRF5_SDK_12.2.0_f012efa.zip](https://developer.nordicsemi.com/nRF5_SDK/nRF5_SDK_v12.x.x/nRF5_SDK_12.2.0_f012efa.zip)

    [nRF5_SDK_12.2.0_offline_doc.zip](https://developer.nordicsemi.com/nRF5_SDK/nRF5_SDK_v12.x.x/nRF5_SDK_12.2.0_offline_doc.zip) 
    ```shell
	unzip nRF5_SDK_12.2.0_f012efa.zip -d ${SDK_ROOT}/
	echo 'GNU_INSTALL_ROOT := /opt/gcc-arm-none-eabi-6_2-2016q4
    GNU_VERSION := 6.2.1
	GNU_PREFIX := arm-none-eabi' > ${SDK_ROOT}/components/toolchain/gcc/Makefile.posix
	unzip -o nRF5_SDK_12.2.0_offline_doc.zip -d ${SDK_ROOT}/documentation/
    cp ParseMakefile4Eclipse.sh ${SDK_ROOT}/examples/
    ```
8. [nRF5x-Command-Line-Tools-Linux64](https://www.nordicsemi.com/eng/nordic/download_resource/51392/20/35191692) 
    ```shell
	tar xvf nRF5x-Command-Line-Tools_9_3_1_Linux-x86_64.tar -C ${SDK_ROOT}/
	sed -i 's/Family = NRF51/Family = NRF52/g' ${SDK_ROOT}/nrfjprog/nrfjprog.ini
    ```
9. [micro-ecc-master.zip](https://codeload.github.com/kmackay/micro-ecc/zip/master)
    ```shell
	unzip micro-ecc-master.zip -d ${SDK_ROOT}/external/micro-ecc/
	ln -fsrn ${SDK_ROOT}/external/micro-ecc/micro-ecc-master \
	    ${SDK_ROOT}/external/micro-ecc/micro-ecc
	cd ${SDK_ROOT}/external/micro-ecc/nrf52_armgcc/armgcc/
	make
    ```
10. Build the blinky (using [nRF52832 DK](https://www.nordicsemi.com/eng/Products/Bluetooth-low-energy/nRF52-DK))
    ```shell
	cd ${SDK_ROOT}/examples/peripheral/blinky/pca10040/blank/armgcc/
	make
    ```
	Make output:
    ```
	mkdir _build
	Compiling file: nrf_log_backend_serial.c
	Compiling file: nrf_log_frontend.c
	Compiling file: app_error.c
	Compiling file: app_error_weak.c
	Compiling file: app_timer.c
	Compiling file: app_util_platform.c
	Compiling file: hardfault_implementation.c
	Compiling file: nrf_assert.c
	Compiling file: sdk_errors.c
	Compiling file: boards.c
	Compiling file: nrf_drv_clock.c
	Compiling file: nrf_drv_common.c
	Compiling file: nrf_drv_uart.c
	Compiling file: nrf_nvic.c
	Compiling file: nrf_soc.c
	Compiling file: main.c
	Compiling file: RTT_Syscalls_GCC.c
	Compiling file: SEGGER_RTT.c
	Compiling file: SEGGER_RTT_printf.c
	Assembling file: gcc_startup_nrf52.S
	Compiling file: system_nrf52.c
	Linking target: _build/nrf52832_xxaa.out
	   text	   data	    bss	    dec	    hex	filename
	   5220	    112	    116	   5448	   1548	_build/nrf52832_xxaa.out
	Preparing: _build/nrf52832_xxaa.hex
	Preparing: _build/nrf52832_xxaa.bin
    ```
11. Program
    ```shell
	nrfjprog --family nRF52 -e
	nrfjprog --family NRF52 --program _build/nrf52832_xxaa.hex
	nrfjprog --family NRF52 -r
    ```
	Or you can do
    ```shell
	make erase flash
    ```

## Build Secure DFU
1. Generate the private key:

    ```shell
    mkdir -p ${SDK_ROOT}/examples/dfu/bootloader_secure/keys
    nrfutil keys generate ${SDK_ROOT}/examples/dfu/bootloader_secure/keys/dfu_private_key.pem
    ```
2. Generate the public key C file
    ```shell
	nrfutil keys display --key pk --format code \
	    ${SDK_ROOT}/examples/dfu/bootloader_secure/keys/dfu_private_key.pem \
	    --out_file ${SDK_ROOT}/examples/dfu/bootloader_secure/dfu_public_key.c
    ```
3. Add the flash_softdevice target rule
    ```shell
	cd ${SDK_ROOT}/examples/dfu/bootloader_secure/pca10040/armgcc/
	grep -q flash_softdevice Makefile || sh -c 'echo "
	# Flash softdevice
	flash_softdevice:
	\t@echo Flashing: s132_nrf52_3.0.0_softdevice.hex
	\tnrfjprog --program \$(SDK_ROOT)/components/softdevice/s132/hex/s132_nrf52_3.0.0_softdevice.hex -f nrf52 --sectorerase
	\tnrfjprog --reset -f nrf52
	" >> Makefile'
    ```
4. Generate the secure DFU firmware
    ```shell
	make -j
    ```
5. Program 
    ```shell
	make erase flash_softdevice flash
    ```
## Build experimental_ble_app_buttonless_dfu

1. Go to the folder containing the patch files
    ```shell
	cd /path/to/INIT_nrf5_sdk/
    ```
2. Fix the gcc linker file
    ```shell
	patch -Nr - ${SDK_ROOT}/examples/ble_peripheral/experimental_ble_app_buttonless_dfu/pca10040/s132/armgcc/ble_app_buttonless_dfu_gcc_nrf52.ld \
	    < ble_app_buttonless_dfu_gcc_nrf52.ld.patch
	patch -Nr - ${SDK_ROOT}/components/ble/ble_services/ble_dfu/ble_dfu.c < ble_dfu.c.patch
    ```
3. compile
    ```shell
	cd ${SDK_ROOT}/examples/ble_peripheral/experimental_ble_app_buttonless_dfu/pca10040/s132/armgcc/
	make -j
    ```
4. Generate the secure DFU ZIP file
    ```shell
	nrfutil pkg generate --hw-version 52 --sd-req 0x8C --application-version 4 \
    --application _build/nrf52832_xxaa.hex --key-file \
    ${SDK_ROOT}/examples/dfu/bootloader_secure/keys/dfu_private_key.pem \
    _build/nrf52832_xxaa.zip
    ```
5. Use the [nRF Connect](https://www.nordicsemi.com/eng/Products/Nordic-mobile-Apps/nRF-Connect-for-mobile-previously-called-nRF-Master-Control-Panel) to upload the firmware zip file.
	1. Connect to DfuTarg.
	2. click DFU logo and select the ZIP file.

6. to re-enable the DFU mode:
	1. use [nRF Connect](https://www.nordicsemi.com/eng/Products/Nordic-mobile-Apps/nRF-Connect-for-mobile-previously-called-nRF-Master-Control-Panel) to connect to Nordic_Template.
	2. Enable Notifications for Experimental Buttonless DFU Service. 
	3. Send Request. 

## Eclipse New Example Project Setup

***When you add / remove SDK libs to your project, you need to modify the Makefile. And you should update the Eclipse project's Paths and Symbols as well as the library file links.***

*The following uses ```${SDK_ROOT}/examples/peripheral/blinky``` as an example:*

1. Setup build environments

    ```shell
	source setenv.sh
	```
2. New --> Project... --> C/C++ --> Makefile Project with Existing Code
    * Browse... and add existing code Makefile location
    * Change "Project Name" to ```blinky```
    * Choose "Cross ARM GCC" toolchain

3. Project --> Properties:
    * C/C++ Build
        * Behavior --> Enable parallel build
    * C/C++ Build --> Settings --> Devices
        * You need to install the pack to see the following feature.
        * Choose: Devices --> Nordic Semiconductor --> nRF52 Series --> nRF52832_xxAA
    * Apply
    * OK

4. In a terminal, use ParseMakefile4Eclipse.sh to extract the contants for Eclipse. 
    * You can always do it manually in Eclipse if you don't understand how it works or don't trust the script.

    ```shell
	cd ${SDK_ROOT}/examples
	./ParseMakefile4Eclipse.sh peripheral/blinky/pca10040/blank/armgcc
	```

5. In Eclipse, File --> Refresh 

6. In Eclipse, Project --> Properties:
    * C/C++ General --> Paths and Symbols
    	* import settings from ```/tmp/nrf.tmp```
7. In Eclipse, Project --> C/C++ Index --> Rebuild